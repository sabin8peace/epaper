<?php

namespace App\Http\Controllers;

use DateTime;
use App\Pages;
use App\Epaper;
use DateTimeZone;
use App\EpaperLink;
use Illuminate\Http\Request;
use Intervention\Image\Image;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function crop()
    {
        return view('crop');
    }
    public function image(Request $request)
    {
        dd($request);
        if($request->hasFile('profile_image')) {
            //get filename with extension
            $filenamewithextension = $request->file('profile_image')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('profile_image')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('profile_image')->storeAs('public/profile_images', $filenametostore);

            if(!file_exists(public_path('storage/profile_images/crop'))) {
                mkdir(public_path('storage/profile_images/crop'), 0755);
            }

            // crop image
            $img = Image::make(public_path('storage/profile_images/'.$filenametostore));
            $croppath = public_path('storage/profile_images/crop/'.$filenametostore);

            $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
            $img->save($croppath);

            // you can save crop image path below in database
            $path = asset('storage/profile_images/crop/'.$filenametostore);

            return redirect('image')->with(['success' => "Image cropped successfully.", 'path' => $path]);
        }
    }


//epaper

    public function epaperdate(Request $request)
    {

        $date=$request->datetime;
        return $this->epaper($date);

    }

    public function epaper($date=null,$page=null)
    {

         if ($date == null && $page == null) {
            $date = new DateTime("now", new DateTimeZone('Asia/Kathmandu'));
            $date = $date->format('Y-m-d 00:00:00');
            $data = Epaper::where('datetime', $date)->orderBy('id', 'asc')->first();
            $epaperimage = Epaper::where('datetime', $date)->orderBy('id', 'asc')->get();

        }
        elseif ($date != null && $page == null)
        {

            // dd($epaperimage);
            $date = strtotime($date);
            $date = date('Y-m-d 00:00:00', $date);
            $data = Epaper::where('datetime', $date)->orderBy('id', 'asc')->first();
            $epaperimage = Epaper::where('datetime', $date)->orderBy('id', 'asc')->get();



        }
        else {
            // dd($date,$page);
            $date = strtotime($date);
            $date = date('Y-m-d 00:00:00', $date);
            $data = Epaper::where('datetime', $date)->where('id', $page)->first();
            $epaperimage = Epaper::where('datetime', $date)->orderBy('id', 'asc')->get();

            // $date = $date->format('Y-m-d 00:00:00');

        }


        return view('frontend/epaper',compact('epaperimage','data'));
    }

//epaper
}
